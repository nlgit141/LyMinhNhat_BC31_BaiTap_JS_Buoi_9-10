// Tạo lớp đối tượng (object NhanVien)
var NhanVien = function (_tk, _ten, _email, _mk, _ngaylam, _luongcoban, _chucvu, _giolam) {
    this.tk = _tk;
    this.ten = _ten;
    this.email = _email;
    this.mk = _mk;
    this.ngaylam = _ngaylam;
    this.luongcoban = _luongcoban;
    this.chucvu = _chucvu;
    this.giolam = _giolam;
    this.tongLuong = function tinhTien() {
        var tinhLuong = 0;
        var chucVu = this.chucvu;
        var luongCoBan = this.luongcoban;
        switch (chucVu) {
            case "sếp": {
                tinhLuong = luongCoBan * 3;
            }
            case "trưởng phòng": {
                tinhLuong = luongCoBan * 2;
            }
            case "nhân viên": {
                tinhLuong = luongCoBan;
            }
                return tinhLuong;
        }
    };

    this.xeploai = function () {
        var xepLoai = this.giolam;
        if (xepLoai >= 192) {
            return "Xuất sắc";
        } else if (xepLoai >= 176) {
            return "Giỏi";
        } else if (xepLoai >= 160) {
            return "khá";
        } else {
            return "Trung bình";
        }
    };
};

// Tạo mảng (arrDSNV) lưu lớp đối tượng (object NhanVien)
var arrDSNV = [];



//lấy jsonDSNV từ localStorage và chuyển về thành data để xuất ra màn hình
var dataJSON = localStorage.getItem("keyDSNV");
if (dataJSON !== null) {
    var arrDSnhanVien = JSON.parse(dataJSON);
    for (var i = 0; i < arrDSnhanVien.length; i++) {
        var item = arrDSnhanVien[i];
        var nv = new NhanVien(item.tk, item.ten, item.email, item.mk, item.ngaylam, item.luongcoban, item.chucvu, item.giolam);
        arrDSNV.push(nv);
    } renderDSNV(arrDSNV);
}

// lưu Data vào localStorage:
var luuLocalStorage = function () {
    //1: chuyển data gốc thành JSON
    var jsonDSNV = JSON.stringify(arrDSNV);

    //2: Lưu JSON vào LocalStorage: đối tượng quản lý vùng nhớ của web
    localStorage.setItem("keyDSNV", jsonDSNV); // BẮT BUỘC truyền vào 2 đối số, đối số đầu tiên là tên của JSON cần lưu =>(KEY) và đối số thứ 2 là jsonDSNV => (value: đã biến Data gốc thành JSON)


};


var layThongTinTuForm = function () {
    var tk = document.getElementById("tknv").value;
    var ten = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var mk = document.getElementById("password").value;
    var ngaylam = document.getElementById("datepicker").value;
    var luongcoban = document.getElementById("luongCB").value * 1;
    var chucvu = document.getElementById("chucvu").value;
    var giolam = document.getElementById("gioLam").value * 1;
    var nv = new NhanVien(tk, ten, email, mk, ngaylam, luongcoban, chucvu, giolam);
    return nv;
};

var hienThiThongTinLenForm = function (nv) {

    document.getElementById("tknv").value = nv.tk;
    document.getElementById("name").value = nv.ten;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.mk;
    document.getElementById("datepicker").value = nv.ngaylam;
    document.getElementById("luongCB").value = nv.luongcoban;
    document.getElementById("chucvu").value = nv.chucvu;
    document.getElementById("gioLam").value = nv.giolam;
    // var nv = new NhanVien(tk, ten, email, mk, ngaylam, luongcoban, chucvu, giolam);
    // return nv;
};



//  duyệt mảng để xuất ra màn hình
function renderDSNV(arrNhanVien) {
    var contenHTML = "";
    for (var index = 0; index < arrNhanVien.length; index++) {
        var nv = arrNhanVien[index];
        var contentTR =
            `<tr>
                <td>${nv.tk}</td>
                <td>${nv.ten}</td>
                <td>${nv.email}</td>
                <td>${nv.ngaylam}</td>
                <td>${nv.chucvu}</td>
                <td> ${Intl.NumberFormat().format(nv.tongLuong()) + "VNĐ"}</td>
                <td>${nv.xeploai()}</td>
                <td>
                    <button data-toggle="modal" data-target="#myModal" onclick="suaNhanVien(${nv.tk})" class="btn btn-primary ">sửa</button>
                    <button onclick="xoaNhanVien(${nv.tk})" class="btn btn-danger">xóa</button>
                </td>
            </tr>`;
        //,${nv.ten},${nv.email},${nv.mk},${nv.ngaylam},${nv.luongcoban},${nv.chucvu},${nv.giolam}
        contenHTML += contentTR;
    } document.getElementById("tableDanhSach").innerHTML = contenHTML;
}

// xuất ra màn hình
document.getElementById("btnThemNV").addEventListener("click", function () {
    var nv = layThongTinTuForm();
    arrDSNV.push(nv);
    renderDSNV(arrDSNV);
    luuLocalStorage();

});

// xoá nhân viên
//
/** tìm vị trí bằng cách dựa vào tk nhân viên
 * 
 * @param {*} tkNhanVien = ${nv.tk}
 * là đối số truyền vào dòng button xóa nhân viên ${nv.tk}
 * 
 * có vị trí => xóa nguyên phần nhân viên cần xóa
 */

//cách 1 dùng vòng lặp:

// function xoaNhanVien(tkNhanVien) {
//     var vitri;
//     console.log('array: ', arrDSNV);
//     for (var index = 0; index < arrDSNV.length; index++) {
//         var item = arrDSNV[index];
//         console.log('item: ', item);
//         if (item.tk == tkNhanVien) {
//             vitri = index;
//             arrDSNV.splice(vitri, 1);
//         }
//     }
//     console.log('item xóa: ', item);
//     console.log('array còn lại: ', arrDSNV);
//     renderDSNV(arrDSNV);
// }

// cách 2 dùng indexOf:
function timKiemViTri(arrDSNV, tkNhanVien) {
    return arrDSNV.findIndex(function (item) {
        return item.tk == tkNhanVien;
    });
}

function xoaNhanVien(tkNhanVien) {
    var viTri = timKiemViTri(arrDSNV, tkNhanVien);
    arrDSNV.splice(viTri, 1);
    renderDSNV(arrDSNV);

    // luuLocalStorage();
}

//Sửa nhân viên:

function suaNhanVien(tkNhanVien) {
    console.log('tkNhanVien: ', tkNhanVien);
    var viTri = timKiemViTri(arrDSNV, tkNhanVien);
    console.log('viTri: ', viTri);
    if (viTri == -1) {
        return;
    }
    var NhanVien = arrDSNV[viTri];
    console.log('index: ', NhanVien);
    hienThiThongTinLenForm(NhanVien);

    // var viTri = timKiemViTri(arrDSNV, tkNhanVien);
    // var viTri = timKiemViTri(arrDSNV, tkNhanVien);
    // viTri = new NhanVien();
    // document.getElementById("tknv").value = nv.tk;
    // document.getElementById("name").value = nv.ten;
    // document.getElementById("email").value = nv.email;
    // document.getElementById("password").value = nv.mk;
    // document.getElementById("datepicker").value = nv.ngaylam;
    // document.getElementById("luongCB").value = nv.luongcoban;
    // document.getElementById("chucvu").value = nv.chucvu;
    // document.getElementById("gioLam").value = nv.giolam;
    // nv = new NhanVien(tk, ten, email, mk, ngaylam, luongcoban, chucvu, giolam);
    // return nv;

}
